#!/bin/sh

DRIVER=bigdatatraining.hadoop.testhbase.testDriver
LIBJARS=$(ls -d -1 $PWD/lib/*.jar | tr "\\n" "," | sed 's/,$//')

export HADOOP_CLASSPATH=$HBASE_CLASSPATH:`echo ${LIBJARS} | sed s/,/:/g`

yarn jar $PWD/driver/*.jar ${DRIVER} -libjars ${LIBJARS} "$@"