/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.testhbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.lang.Object;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;


/**
 *
 * @author stas
 */
public class testDriver extends Configured implements Tool{
 //private Object TableMapReduceUtil;   
    
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new testDriver(), args));
    }
    
    
    
  public int run(String[] args) throws Exception {
 Configuration conf = HBaseConfiguration.create();
Job job = Job.getInstance(conf);
job.setJarByClass(getClass());     // class that contains mapper and reducer

Scan scan = new Scan();
//scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
//scan.setCacheBlocks(false);  // don't set to true for MR jobs
// set other scan attrs

TableMapReduceUtil.initTableMapperJob("test1", scan, testMapper.class, Text.class, IntWritable.class, job);
TableMapReduceUtil.initTableReducerJob("test3", testReducer.class, job);
        
job.setNumReduceTasks(1);   // at least one, adjust as required

    return (job.waitForCompletion(true) ? 0 : 1);
  }

    
}
