/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.invertedindex;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author stas
 */
public class InvertedIndexReduce extends Reducer<Text, Text, Text, Text>{
    @Override
    public void reduce(final Text key, final Iterable<Text> values, final Context context) throws IOException, InterruptedException{
        
        StringBuilder resBuff =new StringBuilder();
        for (Text val:values) {
            resBuff.append(val.toString());
            if (values.iterator().hasNext()){
                resBuff.append(" -> ");
            }
        }
        context.write(key, new Text(resBuff.toString()));
    }
    
}
