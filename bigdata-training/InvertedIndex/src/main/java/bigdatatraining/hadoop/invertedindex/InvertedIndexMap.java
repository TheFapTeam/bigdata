/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.invertedindex;

import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 *
 * @author stas
 */
public class InvertedIndexMap extends Mapper<LongWritable, Text, Text, Text> {
    
    private final static Text word = new Text();
    private final static Text location = new Text();
    private boolean caseSensitive = false;
    
    @Override
    public void map (LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        FileSplit filesplit = (FileSplit) context.getInputSplit();
        String fileName = filesplit.getPath().getName();
        location.set(fileName);
        
        String line = value.toString();
        StringTokenizer tokens = new StringTokenizer(line.toLowerCase());
        while (tokens.hasMoreTokens()){
            word.set(tokens.nextToken());
            context.write(word, location);
        }
    }
    
    @Override
    protected void setup(Context context){
        Configuration conf = context.getConfiguration();
        this.caseSensitive = conf.getBoolean("invertedindex.case.sensitive", caseSensitive);
    }
    
}
