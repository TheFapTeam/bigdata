/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.invertedindex;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *
 * @author stas
 */
public class Driver extends Configured implements Tool{
    
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new Driver(),args));
    }
    
   
    @Override
public int run(String[] args) throws Exception {
        if (args.length != 2) {
             throw new IllegalArgumentException("Usage: Driver <input> <output>");
         }
             
        Job job = Job.getInstance(getConf());
        job.setJobName("InvertedIndex");
        

        job.getConfiguration().set("mapreduce.output.textouputformat.separator", " | ");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        job.setJarByClass(getClass());

        job.setMapperClass(InvertedIndexMap.class);
        job.setReducerClass(InvertedIndexReduce.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        Path inputFilePath = new Path(args[0]);
        Path outputFilePath = new Path(args[1]);


       FileInputFormat.setInputDirRecursive(job, true);

        FileInputFormat.addInputPath(job, inputFilePath);
        FileOutputFormat.setOutputPath(job, outputFilePath);
        
        FileSystem.newInstance(getConf()).delete(outputFilePath, true);
       
        if (job.waitForCompletion(true)) {
            return 0;
        }
       
        return 1;
       
}
        }
