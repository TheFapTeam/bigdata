
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author stas
 */
public class Driver extends Configured implements Tool{
    
    public void main(String[] args) throws Exception{
        System.exit(ToolRunner.run(new Configuration(), new Driver(), args));
       
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            throw new IllegalArgumentException("Usage: Driver <inputTable> <outputTable>");
        }

        Configuration conf = HBaseConfiguration.create();
        Job job = Job.getInstance(conf);
        job.setJarByClass(getClass());

        String inputTableName = args[0];
        String outputTableName = args[1];
        
        TableMapReduceUtil.initTableMapperJob(inputTableName, MyMapper.class, null, null, job);
       // TableMapReduceUtil.initTableReducerJob(, MyReducer.class, job);

        return (job.waitForCompletion(true) ? 0 : 1);
        
        
    
     
    }
    
}
