/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.task3;

import java.io.IOException;
import java.util.List;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author stas
 */
//public class Map extends Mapper<LongWritable, List<Text>, Text, IntWritable>{
public class Map extends Mapper<LongWritable, List<Text>, AvroKey<CharSequence>, AvroValue<CharSequence>> {

    @Override
    public void map(LongWritable key, List<Text> values, Context context) throws IOException, InterruptedException {
        if (!values.isEmpty()) {
            String keyRes;
            String valRes;
            keyRes = values.get(2).toString();
            valRes = values.get(5).toString();
            context.write(new AvroKey<CharSequence>(keyRes), new AvroValue<CharSequence>(valRes));
        }
    }

}
