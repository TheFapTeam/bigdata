#!/bin/sh

DRIVER=bigdatatraining.hadoop.task4.Driver
LIBJARS=$(ls -d -1 $PWD/lib/*.jar | tr "\\n" "," | sed 's/,$//')

export HADOOP_CLASSPATH=`echo ${LIBJARS} | sed s/,/:/g`

yarn jar $PWD/driver/*.jar ${DRIVER} -libjars ${LIBJARS} "$@"