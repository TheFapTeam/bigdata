/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.task3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.BytesWritable;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author stas
 */
public class Reduce extends Reducer<Text, BytesWritable, Text, Text> {

    public void reduce(Text key, Iterable<BytesWritable> values, Context context) throws IOException, InterruptedException {
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(
                "{      "
                + "   \"type\":\"record\","
                + "\"name\":\"mfc\", "
                + "\"fields\": [{    "
                + "	\"name\": \"Id\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"subject_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"mfc_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"address\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Full_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Number_of_windows\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"fio\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"web\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"phone\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\":\"email\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"x\","
                + "	\"type\": \"string\""
                + "}"
                + "]"
                + "}"
        );

        int k = 1;
        String res = "";
        DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(schema);
        Decoder decoder = DecoderFactory.get().binaryDecoder(values.iterator().next().getBytes(), null);
        GenericRecord result = reader.read(null, decoder);
        int max = Integer.parseInt(result.get(5).toString());
        int sum = max;
        for (BytesWritable value : values) {
            DatumReader<GenericRecord> reader1 = new GenericDatumReader<GenericRecord>(schema);
            Decoder decoder1 = DecoderFactory.get().binaryDecoder(value.getBytes(), null);
            GenericRecord result1 = reader1.read(null, decoder1);
            int val = Integer.parseInt(result.get(5).toString());
            k++;
            sum += val;
            if (val > max) {
                max = val;
            }
            res = "\nSum = " + String.valueOf(sum) + " | Max = " + String.valueOf(max) + " | Avg = " + String.valueOf((sum / k)) + "\n";
        }

        context.write(key, new Text(res));

    }

}
