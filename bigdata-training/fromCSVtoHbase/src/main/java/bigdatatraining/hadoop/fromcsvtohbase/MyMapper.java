/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.fromcsvtohbase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author stas
 */
public class MyMapper extends Mapper<LongWritable, List<Text> , IntWritable, BytesWritable> {
    
    @Override
    public void map(LongWritable key, List<Text> values, Context context) throws IOException, InterruptedException {

        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(
                "{      "
                + "   \"type\":\"record\","
                + "\"name\":\"mfc\", "
                + "\"fields\": [{    "
                + "	\"name\": \"Id\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"subject_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"mfc_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"address\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Full_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Number_of_windows\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"fio\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"web\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"phone\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\":\"email\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"x\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"y\","
                + "	\"type\": \"string\""
                + "}"
                + "]"
                + "}"
        );

        if (!values.isEmpty()) {
            String resKey = values.get(0).toString();
            GenericRecord datum = new GenericData.Record(schema);
            int i = 0;
            for (Text value : values) {
                datum.put(i, value.toString());
                i++;
            }
           
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
            Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);
            writer.write(datum, encoder);
            encoder.flush();
            context.write(new IntWritable(Integer.parseInt(resKey)), new BytesWritable(out.toByteArray()));
        }

    }

}
