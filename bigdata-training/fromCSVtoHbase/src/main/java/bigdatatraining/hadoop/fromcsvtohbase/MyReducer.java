/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.fromcsvtohbase;

import java.io.IOException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;

/**
 *
 * @author stas
 */
public class MyReducer extends TableReducer<IntWritable, BytesWritable, Put> {

    public void reduce(IntWritable key, Iterable<BytesWritable> values, Context context) throws IOException, InterruptedException {
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(
                "{      "
                + "   \"type\":\"record\","
                + "\"name\":\"mfc\", "
                + "\"fields\": [{    "
                + "	\"name\": \"Id\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"subject_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"mfc_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"address\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Full_name\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"Number_of_windows\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"fio\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"web\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"phone\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\":\"email\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"x\","
                + "	\"type\": \"string\""
                + "},{"
                + "	\"name\": \"y\","
                + "	\"type\": \"string\""
                + "}"
                + "]"
                + "}"
        );
        
        
        int i =0;
        for (BytesWritable value : values) {
            DatumReader<GenericRecord> reader1 = new GenericDatumReader<GenericRecord>(schema);
            Decoder decoder1 = DecoderFactory.get().binaryDecoder(value.getBytes(), null);
            GenericRecord result1 = reader1.read(null, decoder1);
            Put put = null;
            for (int j=0; j<schema.getFields().size();j++){
            put = new Put(Bytes.toBytes(key.toString()));
            String fieldName = schema.getFields().get(j).name();
            put.add(Bytes.toBytes("data"), Bytes.toBytes(fieldName), Bytes.toBytes(result1.get(j).toString()));
            
            context.write(null, put);}
        }
           
    }

}
