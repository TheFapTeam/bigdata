/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bigdatatraining.hadoop.fromcsvtohbase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.util.LineReader;

/**
 *
 * @author stas
 */
class MyCustomLineReader4CSV extends RecordReader<LongWritable, List<Text>> {

    private long start;
    private long pos;
    private long end;
    private LineReader in;
    private int MaxLineLength;
    private final LongWritable key;
    private final Text value = new Text();
    final RecordReader<LongWritable, Text> textRecordReader;
    int count = 0;

    MyCustomLineReader4CSV(RecordReader<LongWritable, Text> textRecordReader) {
        this.key = new LongWritable();
        this.textRecordReader = textRecordReader;
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        textRecordReader.initialize(split, context);

    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        return textRecordReader.nextKeyValue();
    }

    @Override
    public LongWritable getCurrentKey() throws IOException, InterruptedException {
        return this.textRecordReader.getCurrentKey();
    }

    @Override
    public List<Text> getCurrentValue() throws IOException, InterruptedException {
        Text text = textRecordReader.getCurrentValue();
        String[] tokens = text.toString().split("(\",)|(,\")|(\\d,\\d)");
        count = 0;
        for (String token : tokens) {
            if (!token.isEmpty()) {
                count++;
            }
        }

        List<Text> list = new ArrayList();
        if (count == 12) //if (count != 0) 
        {
            {
                for (String token : tokens) {
                    list.add(new Text(token));
                }
            }
        }
        return list;

    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return this.textRecordReader.getProgress();
    }

    @Override
    public void close() throws IOException {
        this.textRecordReader.close();
    }

}
